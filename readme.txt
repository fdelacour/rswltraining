%% Document name: readme.txt
% Localisation : 
% 
% Description:
%	This about file describe the proposal for project structure document
%   Describe the folders present in the localisation mentionned above.
%  	The description for each folder includes the purpose, ?? and the subdirectories.
%
% Rules:
%	The folder name shall be in capital letters.
%	The folder name shall be prefixed with a letter and a minus (A-).
%   The folders tagged "mandatory" with shall never be deleted.
%	The Resp tagged with "[ ]" is the proposed responsible of the folder.
%
% $Revision: 1.1$
% $Author: M.DELACOUR RSA$
% $Date: September 03, 2018$
% $References: TBD
% $history: 	1.0:first version of the document.
% 				1.0:update directories name and explantion.
%--------------------------------------------------------------------------

A_Project_Description :
-Purpose : Relevant docs to aim the understanding of the project.
-Priority : optional
-Resp : [PM] / PO / SAFETY / CYBER
-Contains :

B_Project_Management :
-Purpose : All documents 
-Priority : mandatory
-Resp : [PM] / PO / SAFETY / CYBER
-Contains :
	-A_Project_Plan
	-B_Project_Schedule
	-C_Open_Points_and_Risk_Assessment
	-D_Meetings

C_Safety_Quality_Management :
-Purpose :
-Priority : mandatory
-Resp : TBD
-Contains :
	-A_Safety_Quality_Plan
	-B_Quality_Schedule
	-C_Quality_Meetings
	-D_Open_Points
	-E_Gate_Status
	-F_Assessment
	-G_Process_Deployment
	-H_Capitalization

D_Tools_and_Methods :
-Purpose :
-Priority : mandatory
-Resp : TBD
-Contains :

E_Software_Development :
-Purpose :
-Priority : mandatory
-Resp : TBD
-Contains :
	-A_Specification
	-B_Architecture
	-C_Detailed_Design
	-D_Implementation
	-E_Integration_Tests
	-F_Qualification_Tests
	-G_Joint_Reviews

F-Product_Releases	:
-Purpose :
-Priority : Optional
-Resp : TBD
-Contains :


Un petit update pour essaie de Fork et merge request  (Atelier 3)
